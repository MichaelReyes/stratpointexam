package com.exam.michael.developerexam.ui.view.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.exam.michael.developerexam.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends BaseFragment {

    @Override
    protected int getFragmentView() {
        return R.layout.fragment_main;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, View view) {
        super.onViewReady(savedInstanceState, view);
    }
}
