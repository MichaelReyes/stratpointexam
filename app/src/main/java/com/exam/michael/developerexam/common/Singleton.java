package com.exam.michael.developerexam.common;

import com.exam.michael.developerexam.model.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 8/27/18.
 */

public class Singleton {

    private static volatile Singleton instance = null;

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }

    public static Singleton deleteInstance() {
        if (instance != null) {
            synchronized (Singleton.class) {
                if (instance != null) {
                    instance = null;
                }
            }
        }
        return instance;
    }

    private List<Movie> movies;

    public List<Movie> getMovies() {
        return movies == null ? new ArrayList<Movie>() : movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Movie getMovieById(long id) {
        if (this.movies != null)
            for (Movie movie : this.movies) {
                if (movie.getId() == id)
                    return movie;
            }

        return null;
    }

    public void updateMovieData(Movie movie){
        if (this.movies != null) {
            for (int i = 0; i < this.movies.size(); i++) {
                Movie mov = this.movies.get(i);

                if(mov.getId() == movie.getId())
                    this.movies.set(i, movie);
            }
        }
    }
}
