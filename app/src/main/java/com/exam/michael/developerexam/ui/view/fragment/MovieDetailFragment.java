package com.exam.michael.developerexam.ui.view.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.exam.michael.developerexam.R;
import com.exam.michael.developerexam.common.Constants;
import com.exam.michael.developerexam.common.Singleton;
import com.exam.michael.developerexam.common.task.LoadImageToViewTask;
import com.exam.michael.developerexam.model.Movie;

public class MovieDetailFragment extends BaseFragment {

    private Movie mMovie;

    @Override
    protected int getFragmentView() {
        return R.layout.fragment_movie_detail;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, View view) {
        super.onViewReady(savedInstanceState, view);

        initializeViews();
    }

    private void initializeViews() {
        Bundle args = getArguments();

        if (args != null && args.containsKey(Constants.ARGS_MOVIE_ID)) {
            mMovie = Singleton.getInstance().getMovieById(args.getLong(Constants.ARGS_MOVIE_ID));
        }

        if (mMovie != null) {
            setupMovieDetails();
        }
    }

    private void setupMovieDetails() {

        final ProgressBar pbBackDrop = mBaseView.findViewById(R.id.movie_detail_pbBackDrop);

        ImageView ivBackDrop = (ImageView) mBaseView.findViewById(R.id.movie_detail_ivBackdrop);
        if (mMovie.getBackdrop() != null) {
            ivBackDrop.setImageBitmap(mMovie.getBackdrop());
            ivBackDrop.setVisibility(View.VISIBLE);
        }else {
            pbBackDrop.setVisibility(View.VISIBLE);
            LoadImageToViewTask loadBackDropTask = new LoadImageToViewTask(ivBackDrop, new LoadImageToViewTask.CallBack() {
                @Override
                public void onImageLoaded(Bitmap bitmap) {
                    pbBackDrop.setVisibility(View.GONE);
                }
            });
            loadBackDropTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{
                    Constants.BASE_URL + "movielist/images/" + mMovie.getSlug() + "-backdrop.jpg"
            });
        }

        ImageView ivCover = (ImageView) mBaseView.findViewById(R.id.movie_detail_ivCover);
        if(mMovie.getCover() != null)
            ivCover.setImageBitmap(mMovie.getCover());
        else {
            LoadImageToViewTask loadCoverTask = new LoadImageToViewTask(ivCover, null);
            loadCoverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{
                    Constants.BASE_URL + "movielist/images/" + mMovie.getSlug() + "-cover.jpg"
            });
        }

        TextView tvTitle = mBaseView.findViewById(R.id.movie_detail_tvTitle);
        tvTitle.setText(mMovie.getTitle());

        TextView tvYear = mBaseView.findViewById(R.id.movie_detail_tvYear);
        tvYear.setText(mMovie.getYear() + "");

        TextView tvRating = mBaseView.findViewById(R.id.movie_detail_tvRating);
        tvRating.setText(mMovie.getRating() + "");

        TextView tvDetail = mBaseView.findViewById(R.id.movie_detail_tvDetail);
        tvDetail.setText(mMovie.getOverview());
    }

    public MovieDetailFragment() {
    }
}
