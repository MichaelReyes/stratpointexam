package com.exam.michael.developerexam.ui.view.activity;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.exam.michael.developerexam.R;

public class PermissionActivity extends AbsRuntimePermission {

    private static final int REQUEST_PERMISSION = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_permission);

        if (Build.VERSION.SDK_INT >= 23) {
            requestAppPermissions(new String[]{
                            Manifest.permission.INTERNET,
                    },
                    R.string.permission_message, REQUEST_PERMISSION);
        } else {

            startLoginRegisterActivity();

        }
    }


    @Override
    public void onPermissionsGranted(int requestCode) {
        //Do anything when permisson granted
        startLoginRegisterActivity();
    }

    private void startLoginRegisterActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MovieListActivity.start(PermissionActivity.this, MovieListActivity.class, true);
            }
        }, 3000);
    }

}
