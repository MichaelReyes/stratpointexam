package com.exam.michael.developerexam.ui.viewmodel;

import android.app.Activity;
import android.util.Log;

import com.exam.michael.developerexam.api.ApiCallBack;
import com.exam.michael.developerexam.api.ApiClient;
import com.exam.michael.developerexam.api.response.ApiResponse;
import com.exam.michael.developerexam.model.Movie;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 8/27/18.
 */

public class MovieList implements ApiCallBack {

    private Activity mContext;
    private CallBack mCallBack;

    public MovieList(Activity context, CallBack callBack) {
        this.mContext = context;
        this.mCallBack = callBack;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onSuccess(String response, Enum callType) {

        if (callType == ApiCallBack.Type.GET_MOVIES)
            mCallBack.onGetMovies(generateMovieListFromResponse(response));
    }

    private List<Movie> generateMovieListFromResponse(String response) {
        List<Movie> movies = new ArrayList<>();

        ApiResponse apiResponse = ApiResponse.parse(response);

        if (apiResponse != null && apiResponse.getData() != null && apiResponse.getData().getMovies() != null)
            movies.addAll(apiResponse.getData().getMovies());

        return movies;
    }

    @Override
    public void onError(String message) {
        this.mCallBack.onError(message);
    }

    @Override
    public void getData(Enum callType, String endpoint, String requestType, String postParams) {
        ApiClient.run(mContext, callType, endpoint, requestType, postParams, this);
    }

    public interface CallBack {
        void onError(String message);

        void onGetMovies(List<Movie> movies);
    }
}

