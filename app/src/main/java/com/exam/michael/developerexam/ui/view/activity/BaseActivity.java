package com.exam.michael.developerexam.ui.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by Michael on 8/26/18.
 */

public abstract class BaseActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        onViewReady(savedInstanceState, getIntent());
    }

    @CallSuper
    protected void onViewReady(Bundle savedInstanceState, Intent intent){
        //For child activities
    }

    protected abstract int getContentView();


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MovieListActivity.start(this, MovieListActivity.class, true, null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(@NonNull Activity activity, Class mClass, boolean finishCurrentActivity) {
        boolean clearTask = Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN;
        Intent intent = new Intent(activity, mClass);
        if (clearTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        activity.startActivity(intent);
        if (finishCurrentActivity)
            activity.finish();

    }

    public static void start(@NonNull Activity activity, Class mClass, boolean finishCurrentActivity, Bundle extras) {
        boolean clearTask = Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN;
        Intent intent = new Intent(activity, mClass);

        if (extras != null)
            intent.putExtras(extras);

        if (clearTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        activity.startActivity(intent);
        if (finishCurrentActivity)
            activity.finish();

    }

    public static void startWithResult(@NonNull Activity activity, Class mClass, int code) {
        boolean clearTask = Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN;
        Intent intent = new Intent(activity, mClass);
        if (clearTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        activity.startActivityForResult(intent, code);

    }

    public Fragment currentFragment;

    public void navigateTo(Enum pageState, Bundle args, int placeholder) {

    }

    public void toggleProgress(boolean show, View layoutProgressDialog, SwipeRefreshLayout swipeRefreshLayout) {
        layoutProgressDialog.setVisibility(show ? View.VISIBLE : View.GONE);

        if(swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }
}
