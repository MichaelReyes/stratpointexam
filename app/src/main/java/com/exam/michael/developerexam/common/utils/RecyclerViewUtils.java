package com.exam.michael.developerexam.common.utils;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.exam.michael.developerexam.common.Constants;

/**
 * Created by Michael Reyes on 27/08/2018.
 */

public class RecyclerViewUtils {

    public static void setLayoutManager(Context context, String type, RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager, int gridSpanCount) {

        recyclerView.setHasFixedSize(true);

        switch (type) {
            case Constants.LAYOUT_GRID:
                layoutManager = new GridLayoutManager(context, gridSpanCount, OrientationHelper.VERTICAL, false);
                break;
            case Constants.LAYOUT_LINEAR:
                layoutManager = new LinearLayoutManager(context, OrientationHelper.VERTICAL, false);
                break;
            case Constants.LAYOUT_STAGGERED:
                layoutManager = new StaggeredGridLayoutManager(gridSpanCount, GridLayoutManager.VERTICAL);
                break;
        }

        recyclerView.setLayoutManager(layoutManager);
    }

    public static void setHorizontalLayoutManager(Context context, String type, RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager, int gridSpanCount) {

        recyclerView.setHasFixedSize(true);

        switch (type) {
            case Constants.LAYOUT_GRID:
                layoutManager = new GridLayoutManager(context, gridSpanCount, OrientationHelper.HORIZONTAL, false);
                break;
            case Constants.LAYOUT_LINEAR:
                layoutManager = new LinearLayoutManager(context, OrientationHelper.HORIZONTAL, false);
                break;
            case Constants.LAYOUT_STAGGERED:
                layoutManager = new StaggeredGridLayoutManager(gridSpanCount, GridLayoutManager.HORIZONTAL);
                break;
        }

        recyclerView.setLayoutManager(layoutManager);
    }

}