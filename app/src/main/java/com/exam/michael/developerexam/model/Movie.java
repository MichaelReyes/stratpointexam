package com.exam.michael.developerexam.model;

import android.graphics.Bitmap;

import com.exam.michael.developerexam.common.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 8/26/18.
 */

public class Movie {

    private List<String> genres;
    private Long id;
    private String imdbCode;
    private String language;
    private String mpaRating;
    private String overview;
    private Double rating;
    private Long runtime;
    private String slug;
    private String state;
    private String title;
    private String titleLong;
    private String url;
    private Long year;
    private Bitmap cover;
    private Bitmap backdrop;

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImdbCode() {
        return imdbCode;
    }

    public void setImdbCode(String imdbCode) {
        this.imdbCode = imdbCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMpaRating() {
        return mpaRating;
    }

    public void setMpaRating(String mpaRating) {
        this.mpaRating = mpaRating;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Long getRuntime() {
        return runtime;
    }

    public void setRuntime(Long runtime) {
        this.runtime = runtime;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleLong() {
        return titleLong;
    }

    public void setTitleLong(String titleLong) {
        this.titleLong = titleLong;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Bitmap getCover() {
        return cover;
    }

    public void setCover(Bitmap cover) {
        this.cover = cover;
    }

    public Bitmap getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(Bitmap backdrop) {
        this.backdrop = backdrop;
    }

    public static List<Movie> parseJsonArray(JSONArray moviesArray){

        List<Movie> movies = new ArrayList<>();

        for (int i = 0; i < moviesArray.length(); i++) {
            JSONObject movieObject = moviesArray.optJSONObject(i);
            if(movieObject != null){
                Movie movie = new Movie();
                movie.setId(movieObject.optLong(Constants.KEY_ID));
                movie.setImdbCode(movieObject.optString(Constants.KEY_IMDB_CODE));
                movie.setLanguage(movieObject.optString(Constants.KEY_LANGUAGE));
                movie.setMpaRating(movieObject.optString(Constants.KEY_MPA_RATING));
                movie.setOverview(movieObject.optString(Constants.KEY_OVERVIEW));
                movie.setRating(movieObject.optDouble(Constants.KEY_RATING));
                movie.setRuntime(movieObject.optLong(Constants.KEY_RUNTIME));
                movie.setSlug(movieObject.optString(Constants.KEY_SLUG));
                movie.setState(movieObject.optString(Constants.KEY_STATE));
                movie.setTitle(movieObject.optString(Constants.KEY_TITLE));
                movie.setTitleLong(movieObject.optString(Constants.KEY_TITLE_LONG));
                movie.setUrl(movieObject.optString(Constants.KEY_URL));
                movie.setYear(movieObject.optLong(Constants.KEY_YEAR));

                movies.add(movie);
            }

        }

        return movies;

    }

}
