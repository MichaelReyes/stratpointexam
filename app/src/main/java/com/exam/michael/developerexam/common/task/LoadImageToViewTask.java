package com.exam.michael.developerexam.common.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoadImageToViewTask extends AsyncTask<String, Void, Bitmap> {
    private WeakReference<ImageView> mImageView;
    private CallBack mCallBack;

    public LoadImageToViewTask(ImageView imageView, CallBack callback) {
        mImageView = new WeakReference<ImageView>(imageView);
        mCallBack = callback;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {

        return getBitMapFromUrl(urls[0]);

    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {

            if(mImageView != null) {

                ImageView imageView = mImageView.get();


                if (imageView != null) {
                    imageView.setImageBitmap(result);
                    imageView.setVisibility(View.VISIBLE);
                }

            }

            if (mCallBack != null)
                mCallBack.onImageLoaded(result);
        }
    }

    private Bitmap getBitMapFromUrl(String imageuri) {
        HttpURLConnection connection = null;

        try {
            URL url = new URL(imageuri);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream is = connection.getInputStream();
            Bitmap mybitmap = BitmapFactory.decodeStream(is);

            return mybitmap;


        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public interface CallBack {
        void onImageLoaded(Bitmap bitmap);
    }

}