package com.exam.michael.developerexam.common.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.exam.michael.developerexam.ui.view.fragment.BaseFragment;

/**
 * Created by Michael Reyes on 27/08/2018.
 */

public class FragmentUtils {

    public static Fragment replaceFragment(FragmentActivity activity, Fragment fragment, int containerViewId) {
        try {
            String backStackName = fragment.getClass().getSimpleName();

            FragmentManager manager = activity.getSupportFragmentManager();

            FragmentTransaction ft = manager.beginTransaction();

            ft.replace(containerViewId, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStackName);
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fragment;
    }

    public static Fragment replaceFragment(FragmentActivity activity, Fragment fragment, int containerViewId, boolean addToBackStack) {
        String tag = fragment.getClass().getSimpleName();
        FragmentManager manager = activity.getSupportFragmentManager();
        Fragment checkFragment = manager.findFragmentByTag(tag);

        if (checkFragment != null) {
            fragment = checkFragment;
        }

        Fragment currentFragment = manager.findFragmentById(containerViewId);

        if (currentFragment != fragment) {
            //boolean fragmentPopped = manager.popBackStackImmediate(tag, 0);
            FragmentTransaction ft = manager.beginTransaction();
            //ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down);

            //if (!fragmentPopped) {
            ft.replace(containerViewId, fragment, tag);
            //}

            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            if (addToBackStack)
                ft.addToBackStack(tag);
            ft.commitAllowingStateLoss();
        }

        return fragment;
    }

}