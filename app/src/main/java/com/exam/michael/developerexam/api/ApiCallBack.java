package com.exam.michael.developerexam.api;

public interface ApiCallBack {

    enum Type { GET_MOVIES }

    void onSuccess(String response, Enum callType);

    void onError(String message);

    void getData(Enum callType,String endpoint, String requestType, String postParams);
}