package com.exam.michael.developerexam.common.enums;

/**
 * Created by Michael on 8/27/18.
 */

public enum  Status {

    ON_CREATE, ON_REFRESH, ON_RESUME, ON_DESTROY

}
