package com.exam.michael.developerexam.api;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.exam.michael.developerexam.R;
import com.exam.michael.developerexam.common.Constants;

/**
 * Created by Michael on 8/26/18.
 */

public class ApiClient {

    public static void run(final Activity activity, final Enum callType , String endpoint, String requestType, String postParams, final ApiCallBack apiCallBack){
        if(endpoint != null){
            if(requestType == null || requestType.equalsIgnoreCase(""))
                requestType = Constants.REQUEST_METHOD_GET;

            MakeApiCallTask apiCallTask = new MakeApiCallTask(activity ,new MakeApiCallTask.CallBack() {
                @Override
                public void onResponse(String response) {

                    if(!response.equalsIgnoreCase(activity.getResources().getString(R.string.error_message_generic_api_error)))
                        apiCallBack.onSuccess(response, callType);
                    else
                        apiCallBack.onError(response);
                }
            });

            apiCallTask.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR,
                    new String[]{
                        Constants.BASE_URL + endpoint,
                            requestType,
                            postParams
                    }
            );

        }
    }

}
