package com.exam.michael.developerexam.ui.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.AdapterView;

import com.exam.michael.developerexam.R;

import com.exam.michael.developerexam.adapter.MoviesRecyclerViewAdapter;
import com.exam.michael.developerexam.api.ApiCallBack;
import com.exam.michael.developerexam.common.Constants;
import com.exam.michael.developerexam.common.Singleton;
import com.exam.michael.developerexam.common.utils.RecyclerViewUtils;
import com.exam.michael.developerexam.common.enums.Status;
import com.exam.michael.developerexam.model.Movie;
import com.exam.michael.developerexam.ui.view.fragment.MovieDetailFragment;
import com.exam.michael.developerexam.ui.viewmodel.MovieList;

import java.util.ArrayList;
import java.util.List;

public class MovieListActivity extends BaseActivity implements MovieList.CallBack {

    private SwipeRefreshLayout srlContents;
    private RecyclerView rvContents;
    private View layoutProgress, parentLayout;

    private boolean mTwoPane;
    private MovieList mMovieList;

    private MoviesRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<Movie> mMovies;

    @Override
    protected int getContentView() {
        return R.layout.activity_movie_list;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        initializeViews();
    }

    private void initializeViews() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mMovieList = new MovieList(MovieListActivity.this, this);

        parentLayout = findViewById(R.id.parentLayout);

        srlContents = (SwipeRefreshLayout) findViewById(R.id.srlContents);
        srlContents.setOnRefreshListener(onRefreshListener);

        rvContents = (RecyclerView) findViewById(R.id.rvContents);
        mAdapter = new MoviesRecyclerViewAdapter(MovieListActivity.this);
        RecyclerViewUtils.setLayoutManager(MovieListActivity.this, Constants.LAYOUT_LINEAR, rvContents, mLayoutManager, 1);
        rvContents.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(onMovieSelectListener);

        layoutProgress = findViewById(R.id.layoutProgressBar);

        if (findViewById(R.id.movie_detail_container) != null) {
            mTwoPane = true;
        }

        getData(Status.ON_CREATE);
    }

    private AdapterView.OnItemClickListener onMovieSelectListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Movie movie = mAdapter.getMovie(position);
            showMovieDetails(movie);
        }
    };

    private void showMovieDetails(Movie movie) {

        Bundle args = new Bundle();
        args.putLong(Constants.ARGS_MOVIE_ID, movie.getId());

        if (mTwoPane) {
            MovieDetailFragment.commitWithNewInstance(MovieDetailFragment.class, MovieListActivity.this, args, R.id.movie_detail_container);
        } else {
            MovieDetailActivity.start(MovieListActivity.this, MovieDetailActivity.class, false, args);
        }
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            getData(Status.ON_REFRESH);
        }
    };

    private void getData(Status status) {

        if (mMovies == null) {
            mMovies = new ArrayList<>();
            mMovies.addAll(Singleton.getInstance().getMovies());
        }

        if (mMovies != null && mMovies.size() > 0 && status != Status.ON_REFRESH)
            setMovies(mMovies);
        else {

            toggleProgress(true, layoutProgress, srlContents);

            mMovieList.getData(
                    ApiCallBack.Type.GET_MOVIES,
                    "movielist/list_movies_page1.json",
                    Constants.REQUEST_METHOD_GET,
                    null
            );
        }
    }

    @Override
    public void onError(String message) {
        toggleProgress(false, layoutProgress, srlContents);

        Snackbar snackbar = Snackbar
                .make(parentLayout, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    @Override
    public void onGetMovies(List<Movie> movies) {

        toggleProgress(false, layoutProgress, srlContents);

        if (mMovies == null)
            mMovies = new ArrayList<>();

        mMovies.clear();
        mMovies.addAll(movies);

        setMovies(mMovies);
    }

    private void setMovies(List<Movie> movies) {
        if (movies != null)
            mAdapter.setMovies(movies);

        Singleton.getInstance().setMovies(movies);
    }
}
