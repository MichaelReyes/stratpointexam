
package com.exam.michael.developerexam.api.response;

import com.exam.michael.developerexam.common.Constants;
import com.exam.michael.developerexam.model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("unused")
public class ApiResponse {

    private Data data;
    private String status;
    private String statusMessage;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public static ApiResponse parse(String response){

        ApiResponse apiResponse = new ApiResponse();

        try {
            JSONObject object = new JSONObject(response);
            apiResponse.setStatus(object.optString(Constants.KEY_STATUS));
            apiResponse.setStatusMessage(object.optString(Constants.KEY_STATUS_MESSAGE));

            JSONObject data = object.optJSONObject(Constants.KEY_DATA);
            apiResponse.data = new Data();
            apiResponse.data.setPageNumber(data.optLong(Constants.KEY_PAGE_NUMBER));
            JSONArray moviesArray = data.optJSONArray(Constants.KEY_MOVIES);
            apiResponse.data.setMovies(Movie.parseJsonArray(moviesArray));


            return apiResponse;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}
