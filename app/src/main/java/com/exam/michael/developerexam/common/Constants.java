package com.exam.michael.developerexam.common;

public class Constants {

    public static final String BASE_URL = "https://aacayaco.github.io/";

    public static final String LAYOUT_GRID = "grid";
    public static final String LAYOUT_LINEAR = "linear";
    public static final String LAYOUT_STAGGERED = "staggered";

    public static final String ARGS_MOVIE = "_movie";
    public static final String ARGS_MOVIE_ID = "_movie_id";

    public static final String KEY_API_ERROR = "_error";
    public static final String KEY_API_SUCCESS = "_success";

    public static final String REQUEST_METHOD_POST = "_post";
    public static final String REQUEST_METHOD_GET = "_get";

    public static final String KEY_STATUS = "status";
    public static final String KEY_STATUS_MESSAGE = "status_message";
    public static final String KEY_DATA = "data";
    public static final String KEY_PAGE_NUMBER = "page_number";
    public static final String KEY_MOVIES = "movies";
    public static final String KEY_ID = "id";
    public static final String KEY_RATING = "rating";
    public static final String KEY_GENRES = "genres";
    public static final String KEY_LANGUAGE = "language";
    public static final String KEY_TITLE = "title";
    public static final String KEY_URL = "url";
    public static final String KEY_TITLE_LONG = "title_long";
    public static final String KEY_IMDB_CODE = "imdb_code";
    public static final String KEY_STATE = "state";
    public static final String KEY_YEAR = "year";
    public static final String KEY_RUNTIME = "runtime";
    public static final String KEY_OVERVIEW = "overview";
    public static final String KEY_SLUG = "slug";
    public static final String KEY_MPA_RATING = "mpa_rating";


}