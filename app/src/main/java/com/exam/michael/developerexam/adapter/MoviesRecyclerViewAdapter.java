package com.exam.michael.developerexam.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exam.michael.developerexam.R;
import com.exam.michael.developerexam.common.Constants;
import com.exam.michael.developerexam.common.Singleton;
import com.exam.michael.developerexam.common.task.LoadImageToViewTask;
import com.exam.michael.developerexam.model.Movie;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Michael on 08/27/2018.
 */

public class MoviesRecyclerViewAdapter extends CustomRecyclerViewAdapter {

    private Activity mActivity;
    private List<Movie> movies;

    public MoviesRecyclerViewAdapter(Activity activity) {
        this.mActivity = activity;
        this.movies = new ArrayList<>();
    }

    public void setMovies(List<Movie> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public void addMovie(Movie movie) {
        this.movies.add(movie);
        notifyDataSetChanged();
    }

    public List<Movie> getMovies() {
        return this.movies;
    }

    public void updateMovie(Movie movie) {
        for (int i = 0; i < movies.size(); i++) {
            if (movies.get(i).getId() == movie.getId()) {
                movies.set(i, movie);
                notifyDataSetChanged();
            }
        }
    }

    public void clearMovies() {
        this.movies.clear();
        notifyDataSetChanged();
    }

    public Movie getMovie(int position) {
        return movies.get(position);
    }

    @Override
    public CustomRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity)
                .inflate(R.layout.movie_list_content, parent, false);
        Holder dataObjectHolder = new Holder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CustomRecycleViewHolder holder, final int position) {
        final Holder myHolder = (Holder) holder;

        Movie movie = movies.get(position);
        myHolder.tvTitle.setText(movie.getTitle());
        myHolder.tvYearReleased.setText("Released: " + movie.getYear());

        myHolder.ivCover.setVisibility(View.GONE);

        if (movie.getBackdrop() == null)
            myHolder.loadImageToView(
                    Constants.BASE_URL + "movielist/images/" + movie.getSlug() + "-backdrop.jpg",
                    position,
                    myHolder.ivCover
            );
        else {
            myHolder.ivCover.setImageBitmap(movie.getBackdrop());
            myHolder.ivCover.setVisibility(View.VISIBLE);
        }

        if(movie.getCover() == null)
            myHolder.loadImageToView(
                    Constants.BASE_URL + "movielist/images/" + movie.getSlug() + "-cover.jpg",
                    position,
                    null
            );

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class Holder extends CustomRecycleViewHolder {

        ImageView ivCover;
        TextView tvTitle;
        TextView tvYearReleased;

        Holder(View view) {
            super(view);

            ivCover = view.findViewById(R.id.Movie_ivCover);
            tvTitle = view.findViewById(R.id.Movie_tvTitle);
            tvYearReleased = view.findViewById(R.id.Movie_tvYearReleased);

        }

        void loadImageToView(final String url, final int position, ImageView imageView) {
            LoadImageToViewTask loadImageToViewTask = new LoadImageToViewTask(imageView,
                    new LoadImageToViewTask.CallBack() {
                        @Override
                        public void onImageLoaded(Bitmap bitmap) {
                            Movie movie = movies.get(position);
                            if (url.toLowerCase().contains("-backdrop.jpg"))
                                movie.setBackdrop(bitmap);
                            else
                                movie.setCover(bitmap);
                            Singleton.getInstance().updateMovieData(movie);
                        }
                    });
            loadImageToViewTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{url});
        }

    }

}
