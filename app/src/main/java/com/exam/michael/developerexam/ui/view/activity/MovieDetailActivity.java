package com.exam.michael.developerexam.ui.view.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.widget.ImageView;

import com.exam.michael.developerexam.R;
import com.exam.michael.developerexam.common.Constants;
import com.exam.michael.developerexam.common.Singleton;
import com.exam.michael.developerexam.common.task.LoadImageToViewTask;
import com.exam.michael.developerexam.model.Movie;
import com.exam.michael.developerexam.ui.view.fragment.MovieDetailFragment;

public class MovieDetailActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.activity_movie_detail;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);

        initializeViews(intent);
    }

    private void initializeViews(Intent intent) {

        Toolbar toolbar = (Toolbar) findViewById(R.id.movie_detail_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle extras = intent.getExtras();

        MovieDetailFragment.commit(MovieDetailFragment.class, MovieDetailActivity.this, extras, R.id.movie_detail_container);

    }
}
