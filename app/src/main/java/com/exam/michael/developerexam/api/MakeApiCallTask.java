package com.exam.michael.developerexam.api;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.exam.michael.developerexam.R;
import com.exam.michael.developerexam.common.Constants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MakeApiCallTask extends AsyncTask<String,Void,String> {

    private CallBack mCallBack;
    private Activity mActivity;

    public MakeApiCallTask(Activity activity,CallBack callBack) {
        mActivity = activity;
        mCallBack = callBack;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        InputStream inputStream = null;
        String url = params[0];

        String result;

        if (params[1].equals(Constants.REQUEST_METHOD_POST)) {

            inputStream = processPostRequest(url, params[2]);

        } else {

            inputStream = processGetRequest(url);
        }
        if (inputStream != null) {
            result = ConvertStreamToString(inputStream);
        } else {
            result = mActivity.getResources().getString(R.string.error_message_generic_api_error);
        }

        return result;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(mCallBack != null)
            mCallBack.onResponse(result);

    }

    InputStream processGetRequest(String ServerURL) {

        InputStream DataInputStream = null;
        try {

            URL url = new URL(ServerURL);
            HttpURLConnection cc = (HttpURLConnection)
                    url.openConnection();
            cc.setReadTimeout(5000);
            cc.setConnectTimeout(5000);
            cc.setRequestMethod("GET");
            cc.setDoInput(true);

            int response = cc.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                DataInputStream = cc.getInputStream();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return DataInputStream;

    }

    InputStream processPostRequest(String ServerURL, String postParam) {

        InputStream DataInputStream = null;
        try {

            URL url = new URL(ServerURL);

            HttpURLConnection cc = (HttpURLConnection)
                    url.openConnection();
            cc.setReadTimeout(5000);
            cc.setConnectTimeout(5000);
            cc.setRequestMethod("POST");
            cc.setDoInput(true);
            cc.connect();

            DataOutputStream dos = new DataOutputStream(cc.getOutputStream());
            dos.writeBytes(postParam);
            dos.flush();
            dos.close();

            int response = cc.getResponseCode();

            if(response == HttpURLConnection.HTTP_OK) {
                DataInputStream = cc.getInputStream();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return DataInputStream;

    }

    String ConvertStreamToString(InputStream stream) {

        InputStreamReader isr = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder response = new StringBuilder();

        String line = null;
        try {

            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                stream.close();

            } catch (IOException e) {
                e.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response.toString();


    }

    public interface CallBack{

        void onResponse(String response);
    }

}