
package com.exam.michael.developerexam.api.response;

import com.exam.michael.developerexam.model.Movie;

import java.util.List;

@SuppressWarnings("unused")
public class Data {

    private List<Movie> movies;
    private Long pageNumber;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Long getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Long pageNumber) {
        this.pageNumber = pageNumber;
    }

}
