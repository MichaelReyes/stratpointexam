package com.exam.michael.developerexam.ui.view.fragment;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.exam.michael.developerexam.common.utils.FragmentUtils;
import com.exam.michael.developerexam.ui.view.activity.BaseActivity;
import com.exam.michael.developerexam.ui.view.callback.BaseInterface;

/**
 * Created by Michael on 8/27/18.
 */

public abstract class BaseFragment extends Fragment {

    public BaseInterface mCallBack;
    public BaseActivity mActivity;
    public View mBaseView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBaseView = inflater.inflate(getFragmentView(), container, false);
        return mBaseView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewReady(savedInstanceState, view);
    }

    @CallSuper
    protected void onViewReady(Bundle savedInstanceState, View view){
        //For child fragments
    }

    protected abstract int getFragmentView();

    public void showActionBar(String title, boolean show) {
        if (mActivity != null) {
            android.support.v7.app.ActionBar actionBar = mActivity.getSupportActionBar();
            actionBar.setTitle(title);

            if (show)
                actionBar.show();
            else
                actionBar.hide();
        }

    }

    public static <T extends BaseFragment> T newInstance(Class<T> mClass, Bundle args) {
        try {
            T instance = mClass.newInstance();
            instance.setArguments(args);
            return instance;
        } catch (Exception e) {
            /**
             * Error thrown
             */
        }

        return null;
    }

    public static <T extends BaseFragment> T newInstance(Class<T> mClass, Bundle args, BaseInterface baseInterface) {
        try {
            T instance = mClass.newInstance();
            instance.mCallBack = baseInterface;
            instance.setArguments(args);
            return instance;
        } catch (Exception e) {
            /**
             * Error thrown
             */
        }

        return null;
    }

    public static <T extends BaseFragment> Fragment commitWithNewInstance(Class<T> mClass, FragmentActivity activity, Bundle args, int containerViewId) {

        try {
            T instance = mClass.newInstance();

            Fragment fragment = FragmentUtils.replaceFragment(activity, instance, containerViewId/*, false*/);

            if (args != null)
                fragment.setArguments(args);

            return fragment;
        } catch (Exception e) {
            /**
             * Error thrown
             */
        }

        return null;
    }

    public static <T extends BaseFragment> Fragment commit(Class<T> mClass, FragmentActivity activity, Bundle args, int containerViewId) {

        try {
            T instance = mClass.newInstance();

            Fragment fragment = FragmentUtils.replaceFragment(activity, instance, containerViewId, true);

            if (args != null)
                fragment.setArguments(args);

            return fragment;
        } catch (Exception e) {
            /**
             * Error thrown
             */
        }

        return null;
    }

    public void toggleProgress(boolean show, View layoutProgressDialog) {
        layoutProgressDialog.setVisibility(show ? View.VISIBLE : View.GONE);

    }


}